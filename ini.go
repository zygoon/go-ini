// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package ini implements encoding and decoding of Ini-files. The mapping
// between Ini and Go values is described in the documentation for the Marshal
// and Unmarshal functions.
package ini

import (
	"bytes"
)

// Marshal marshals a value to the INI representation.
//
// Only structures and non-nil pointers to structures can be marshaled. For all
// the fields of a structure, the following rules apply:
//
//   - Fields are visited in order in which they are declared.
//   - Only exported fields are considered.
//   - Only fields with the "ini" struct tag are considered.
//   - The struct tag must define the name of the field, for example `ini:"foo"`.
//   - The struct tag may define the section name of the field, for example:
//     `ini:"foo,[section]"`.
//   - The section name "sticks" to subsequent structure fields, until defined
//     again, making it easier to express groups sharing the same section easily.
//   - The struct tag may indicate that zero-values are skipped by using
//     the "omitempty" flag, for example: `ini:"foo,omitempty"`.
//   - Fields must be of one of the basic types listed below, or must implement
//     the encoding.TextMarshaler interface.
//
// Basic types supported directly are:
//   - bool
//   - {,u}int{,8,16,32,64}
//   - float{32,64}
//   - complex{64,128}
//   - string (see below for information about quoting)
//
// Out of compound types, only slices of basic types listed above or slice
// of type that implements TextMarshaler and TextUnmarshaler are supported.
//
// All of those types are converted to strings using functions of the strconv
// package. Strings can be optionally marshaled to a quoted representation,
// using strconv.Quote, when the "quote" struct tag is present.
func Marshal(v interface{}) ([]byte, error) {
	var buf bytes.Buffer
	err := NewEncoder(&buf).Encode(v)

	return buf.Bytes(), err
}

// Unmarshal unmarshals a value from the INI representation.
//
// Marshaled content can be unmarshaled only to non-nil pointers to structures.
//
// Lines starting with ';' or '#' are treated as comments and skipped. Lines
// ending with '\' are joined together before parsing, with the trailing '\'
// replaced by a single space.  This happens after comment elision, allowing
// comments to be placed in the middle of multi-line entry.
//
// Lines starting with '[' and ending with ']' indicate sections. Section
// matching is case-insensitive. Unknown sections are ignored.
//
// Lines containing '=' indicate Field=Value pairs. Field name cannot be
// empty. Field matching is case-insensitive. Unknown fields are ignored. Field
// value is decoded according to the type of the corresponding structure field.
// If the type of structure field implements the encoding.TextUnmarshaler
// interface, that method takes priority. Otherwise, the value is decoded using
// the appropriate function from the strconv package, either ParseBool,
// ParseInt, ParseUint or ParseFloat. Numeric types use bit-size matching that
// of the structure field type. Structure fields of string type are stored
// directly, except when the "quote" struct tag is present, when strconv.Unquote
// is used to obtain the final value.
//
// Fields are assigned in the order they are encountered in the document. When
// the same field is present multiple times, it is decoded normally, overwriting
// earlier values.
//
// For more control over the unmarshal process, use the Decoder directly.
func Unmarshal(data []byte, v interface{}) error {
	return NewDecoder(bytes.NewBuffer(data)).Decode(v)
}
