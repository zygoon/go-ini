// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
// SPDX-FileCopyrightText: Zygmunt Krynicki

package ini

import (
	"bufio"
	"encoding"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"
)

// Encoder writes INI value to an output stream.
type Encoder struct {
	w io.Writer

	commentFunc func(section, field string) string
	commentRune rune

	wrapColumn int
}

// NewEncoder returns a new encoder that writes to w.
func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w: w}
}

// SetCommentFunc sets a function that returns a comment for a given section and field.
//
// The function f is called multiple times during each call to Encoder.Encode.
// The two arguments are used to describe which comment is requested:
//   - global comment is requested with empty section and field names.
//   - per-section comment is requested with non-empty section name and empty field name.
//   - per-field comment is requested with non-empty section and field name.
//
// The returned comment should not contain the rune that introduces the comments
// in INI files ('#' or ';').
//
// Call UseUnixComments or UseWindowsComments to set the preferred comment rune.
// Call SetWrapColumn to control comment wrapping behavior.
func (enc *Encoder) SetCommentFunc(f func(section, field string) string) {
	enc.commentFunc = f
}

// SetWrapColumn sets the column number to use for wrapping text.
//
// At present only comments are wrapped.
//
// The zero value disables wrapping algorithm.
// Values smaller than zero are interpreted as zero.
//
// The customary value is 78 columns. This includes the rune
// introducing the comment and the following space.
func (enc *Encoder) SetWrapColumn(l int) {
	enc.wrapColumn = l
}

// UseUnixComments makes the encoder emit comments introduced by '#'.
func (enc *Encoder) UseUnixComments() {
	enc.commentRune = '#'
}

// UseWindowsComments makes the encoder emit comments introduced by ';'.
func (enc *Encoder) UseWindowsComments() {
	enc.commentRune = ';'
}

// Encode encodes a structure to the INI representation.
func (enc *Encoder) Encode(v any) error {
	if v == nil {
		return ErrEncodeNil
	}

	structValue := reflect.ValueOf(v)
	if structValue.Kind() == reflect.Ptr {
		structValue = reflect.Indirect(structValue)
	}

	structType := structValue.Type()

	if structType.Kind() != reflect.Struct {
		return fmt.Errorf("cannot encode values of type: %T", v)
	}

	iniFieldSlice, err := iniSliceOfType(structType)
	if err != nil {
		return err
	}

	lastSection := ""

	if err := enc.encodeComment("", ""); err != nil {
		return err
	}

	for _, iniFieldInfo := range iniFieldSlice {
		fieldValue := structValue.FieldByIndex(iniFieldInfo.FieldIndex)
		if fieldValue.IsZero() && iniFieldInfo.OmitEmpty {
			continue
		}

		if iniFieldInfo.Section != lastSection {
			if err := enc.encodeComment(iniFieldInfo.Section, ""); err != nil {
				return err
			}

			if _, err := fmt.Fprintf(enc.w, "[%s]\n", iniFieldInfo.Section); err != nil {
				return err
			}

			lastSection = iniFieldInfo.Section
		}

		if err := enc.encodeComment(iniFieldInfo.Section, iniFieldInfo.Name); err != nil {
			return err
		}

		if err := enc.encodeField(fieldValue, &iniFieldInfo); err != nil {
			return err
		}
	}

	return nil
}

// encodeField encodes the value of a given field to the INI representation.
//
// The a single field may be encoded to either one or zero-or-more INI key=value
// entries. Values that implement the TextMarshaler interface are encoded to a
// single key=value entry. Values of slice types are encoded to zero-or-more
// key=value entries. Values of basic types are encoded to one key=value entry,
// possibly using the TextMarshaler interface to obtain the value.
func (enc *Encoder) encodeField(v reflect.Value, info *iniFieldInfo) error {
	const fmtKV = "%s=%s\n"

	// If the value implements TextMarshaler then the entire value is marshaled
	// to a single entry in the ini file.
	if v.CanInterface() {
		if v, ok := v.Interface().(encoding.TextMarshaler); ok {
			data, err := v.MarshalText()

			if err != nil {
				return &EncoderError{
					StructName: info.StructName,
					FieldName:  info.FieldName,
					Err:        err,
				}
			}

			_, err = fmt.Fprintf(enc.w, fmtKV, info.Name, data)

			return err
		}
	}

	// If the value is a slice then marshal individual items as separate entries.
	if v.Kind() == reflect.Slice {
		for i := 0; i < v.Len(); i++ {
			data, err := enc.encodeFieldValue(v.Index(i), info)
			if err != nil {
				return &EncoderError{
					StructName: info.StructName,
					FieldName:  info.FieldName,
					Err:        fmt.Errorf("cannot encode item at index %d: %w", i, err),
				}
			}

			if _, err := fmt.Fprintf(enc.w, fmtKV, info.Name, data); err != nil {
				return err
			}
		}

		return nil
	}

	// Scalar value.
	data, err := enc.encodeFieldValue(v, info)
	if err != nil {
		return &EncoderError{
			StructName: info.StructName,
			FieldName:  info.FieldName,
			Err:        err,
		}
	}

	_, err = fmt.Fprintf(enc.w, fmtKV, info.Name, data)

	return err
}

// encodeFieldValue encodes and returns the value of a single field and returns it.
func (enc *Encoder) encodeFieldValue(v reflect.Value, info *iniFieldInfo) ([]byte, error) {
	if v.CanInterface() {
		if v, ok := v.Interface().(encoding.TextMarshaler); ok {
			return v.MarshalText()
		}
	}

	var valueText string

	switch kind := v.Kind(); kind {
	case reflect.Bool:
		valueText = strconv.FormatBool(v.Bool())
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		valueText = strconv.FormatInt(v.Int(), 10)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		valueText = strconv.FormatUint(v.Uint(), 10)
	case reflect.Float32, reflect.Float64:
		bitSize := 32
		if kind == reflect.Float64 {
			bitSize = 64
		}

		valueText = strconv.FormatFloat(v.Float(), 'f', -1, bitSize)
	case reflect.Complex64, reflect.Complex128:
		bitSize := 64
		if kind == reflect.Complex128 {
			bitSize = 128
		}

		valueText = strconv.FormatComplex(v.Complex(), 'f', -1, bitSize)
	case reflect.String:
		valueText = v.String()
		if info.Quote {
			valueText = strconv.Quote(valueText)
		} else {
			// Multi-line strings are both wrapped at newlines and indented to look nice.
			prefix := strings.Repeat(" ", len(info.Name)+1)
			valueText = strings.ReplaceAll(valueText, "\n", "\\\n"+prefix)
		}
	default:
		// This can never happen if iniSliceOfType is in sync.
		return nil, fmt.Errorf("cannot encode value of unsupported kind %v", kind)
	}

	return []byte(valueText), nil
}

// encodeComment encodes the comment for the given element.
//
// It may be called in one of three ways:
//   - empty section and field name for file-wide comment
//   - non-empty section name and empty field name for section-wide comment
//   - non-empty section and field name for field specific comment.
//
// It may be safely called even if SetCommentFunc was not called before.
func (enc *Encoder) encodeComment(section, field string) error {
	if enc.commentFunc == nil {
		return nil
	}

	c := enc.commentFunc(section, field)
	if c == "" {
		return nil
	}

	commentRune := enc.commentRune
	if commentRune == rune(0) {
		commentRune = ';'
	}

	wrapColumn := enc.wrapColumn
	if wrapColumn < 0 {
		wrapColumn = 0
	}

	writeBuf := bufio.NewWriter(enc.w)
	lineLen := 0

	sc := bufio.NewScanner(strings.NewReader(c))
	if wrapColumn > 0 {
		sc.Split(bufio.ScanWords)
	}

	for sc.Scan() {
		tok := sc.Bytes()

		// If writing this token would exceed the wrap limit, or if
		// wrapping is line-based (wrapColumn is zero), then emit a newline.
		if lineLen > 0 && lineLen+1+len(tok) > wrapColumn {
			if _, err := writeBuf.WriteRune('\n'); err != nil {
				return err
			}

			lineLen = 0
		}

		// Inject the comment rune whenever we write on a new line.
		if lineLen == 0 {
			if n, err := writeBuf.WriteRune(commentRune); err != nil {
				return err
			} else {
				lineLen += n
			}
		}

		if n, err := writeBuf.WriteRune(' '); err != nil {
			return err
		} else {
			lineLen += n
		}

		if n, err := writeBuf.Write(tok); err != nil {
			return err
		} else {
			// TODO: this is simplistic and only really works for ASCII.
			// Interpret the token to understand rendered glyph length.
			lineLen += n
		}
	}

	if err := sc.Err(); err != nil {
		return err
	}

	if _, err := writeBuf.WriteRune('\n'); err != nil {
		return err
	}

	return writeBuf.Flush()
}
