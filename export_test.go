// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ini

var (
	IniSliceOfType = iniSliceOfType
	IniMapOfType   = iniMapOfType
)
