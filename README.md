<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Zygmunt Krynicki
-->

# About the Go Ini encoder/decoder module

This module provides support for marshaling and unmarshaling Go values into
ini-style text format, in the manner similar to how the standard library
`encoding/json` package. Ini files are ubiquitous, easy to edit and easy to
understand given their constrained feature set, with `[sections]` and
line-by-line `key=value` pairs.

Documentation can be found at https://pkg.go.dev/gitlab.com/zygoon/go-ini

## Contributions

Contributions are welcome. Please try to respect Go requirements (1.20 at the
moment) and the overall coding and testing style.

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making it
easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
