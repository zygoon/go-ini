// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ini_test

import (
	"reflect"
	"testing"

	"gitlab.com/zygoon/go-ini"
	"gitlab.com/zygoon/go-ini/internal/testutil"
)

func TestIniSliceOfType_UnsupportedStructTags(t *testing.T) {
	var value struct {
		Field int `ini:"field,magic"`
	}

	_, err := ini.IniSliceOfType(reflect.TypeOf(value))
	testutil.ErrorMatches(t, err, `ini error: field Field contains unsupported struct-tag: "magic"`)
}

func TestIniSliceOfType_MissingFieldName(t *testing.T) {
	var value struct {
		Field int `ini:""`
	}

	_, err := ini.IniSliceOfType(reflect.TypeOf(value))
	testutil.ErrorMatches(t, err, `ini error: field Field lacks struct-tag with field name`)
}

func TestIniSliceOfType_TagParsing(t *testing.T) {
	var value struct {
		internal  int    `ini:"internal,[section]"`
		Exported  int    `ini:"Exported"`
		OmitEmpty int    `ini:"OmitEmpty,omitempty"`
		Quoted    string `ini:"Quoted,quote"`
		Ignore    string // No struct tag, field gets ignored
		Ignore2   string `ini:"-"` // Explicitly ignored
	}

	slice, err := ini.IniSliceOfType(reflect.TypeOf(value))
	testutil.Ok(t, err)
	testutil.SliceHasLen(t, slice, 3)
	testutil.Equal(t, slice[0].Name, "Exported")
	testutil.Equal(t, slice[0].Section, "")
	testutil.Equal(t, slice[0].Quote, false)
	testutil.Equal(t, slice[1].Name, "OmitEmpty")
	testutil.Equal(t, slice[1].Section, "")
	testutil.Equal(t, slice[1].Quote, false)
	testutil.Equal(t, slice[2].Name, "Quoted")
	testutil.Equal(t, slice[2].Section, "")
	testutil.Equal(t, slice[2].Quote, true)
}

func TestIniSliceOfType_SectionTracking(t *testing.T) {
	var value struct {
		internal  int    `ini:"internal,[ignored]"`
		Exported  int    `ini:"Exported,[section]"`
		OmitEmpty int    `ini:"OmitEmpty,omitempty"`
		Quoted    string `ini:"Quoted,quote,[another]"`
	}

	slice, err := ini.IniSliceOfType(reflect.TypeOf(value))
	testutil.Ok(t, err)
	testutil.SliceHasLen(t, slice, 3)
	testutil.Equal(t, slice[0].Section, "section")
	testutil.Equal(t, slice[1].Section, "section")
	testutil.Equal(t, slice[2].Section, "another")
}

func TestIniMapOfType_CaseSensitivity(t *testing.T) {
	var value struct {
		Field int `ini:"FIELD,[SECTION]"`
	}

	const caseSensitive = false

	slice, idxMap, err := ini.IniMapOfType(reflect.TypeOf(value), caseSensitive)
	testutil.Ok(t, err)
	testutil.SliceHasLen(t, slice, 1)
	testutil.Equal(t, slice[0].Section, "SECTION")
	testutil.Equal(t, slice[0].Name, "FIELD")
	testutil.MapHasLen(t, idxMap, 1)
	testutil.MapHasLen(t, idxMap["section"], 1)
	testutil.Equal(t, idxMap["section"]["field"], 0)

	slice, idxMap, err = ini.IniMapOfType(reflect.TypeOf(value), !caseSensitive)
	testutil.Ok(t, err)
	testutil.SliceHasLen(t, slice, 1)
	testutil.Equal(t, slice[0].Section, "SECTION")
	testutil.Equal(t, slice[0].Name, "FIELD")
	testutil.MapHasLen(t, idxMap, 1)
	testutil.MapHasLen(t, idxMap["SECTION"], 1)
	testutil.Equal(t, idxMap["SECTION"]["FIELD"], 0)
}

type neitherMarshalNorUnmarshal struct{}

func TestIniSliceOfType_CustomTypeNeitherMarshalNorUnmarshal(t *testing.T) {
	var value struct {
		NeitherMarshalNorUnmarshal neitherMarshalNorUnmarshal `ini:"NeitherMarshalNorUnmarshal"`
	}

	_, err := ini.IniSliceOfType(reflect.TypeOf(value))
	// XXX: error message doesn't mention TextUnmarshaler even though it is also missing.
	testutil.ErrorMatches(t, err, `ini error: field NeitherMarshalNorUnmarshal non-basic type does not implement encoding.TextMarshaler`)
}

type onlyMarshal struct{}

func (onlyMarshal) MarshalText() ([]byte, error) {
	return []byte(""), nil
}

func TestIniSliceOfType_CustomTypeOnlyMarshal(t *testing.T) {
	var value struct {
		OnlyMarshal onlyMarshal `ini:"OnlyMarshal"`
	}

	_, err := ini.IniSliceOfType(reflect.TypeOf(value))
	testutil.ErrorMatches(t, err, `ini error: field OnlyMarshal non-basic type does not implement encoding.TextUnmarshaler`)
}

type onlyUnmarshal struct{}

func (o *onlyUnmarshal) UnmarshalText(_ []byte) error {
	return nil
}

func TestIniSliceOfType_CustomTypeOnlyUnmarshal(t *testing.T) {
	var value struct {
		OnlyUnmarshal onlyUnmarshal `ini:"OnlyUnmarshal"`
	}

	_, err := ini.IniSliceOfType(reflect.TypeOf(value))
	testutil.ErrorMatches(t, err, `ini error: field OnlyUnmarshal non-basic type does not implement encoding.TextMarshaler`)
}

type bothMarshalAndUnmarshal struct{}

func (bothMarshalAndUnmarshal) MarshalText() ([]byte, error) {
	return []byte(""), nil
}

func (o *bothMarshalAndUnmarshal) UnmarshalText(_ []byte) error {
	return nil
}

func TestIniSliceOfType_CustomTypeBothMarshalAndUnmarshal(t *testing.T) {
	var value struct {
		BothMarshalAndUnmarshal bothMarshalAndUnmarshal `ini:"BothMarshalAndUnmarshal"`
	}

	_, err := ini.IniSliceOfType(reflect.TypeOf(value))
	testutil.Ok(t, err)
}
