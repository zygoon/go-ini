// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package testutil provides utilities for writing unit tests.
package testutil

import (
	"testing"
)

// Equal ensures that arguments compare equal.
func Equal[T comparable](t *testing.T, a, b T) {
	t.Helper()

	if a != b {
		t.Fatalf("Arguments differ %v != %v", a, b)
	}
}

// NotEqual ensures that arguments are not equal.
func NotEqual[T comparable](t *testing.T, a, b T) {
	t.Helper()

	if a == b {
		t.Fatalf("Arguments equal, both %v", a)
	}
}
