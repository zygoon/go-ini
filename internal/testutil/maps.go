// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package testutil

import (
	"testing"
)

// MapHasLen ensures that the argument has given length.
func MapHasLen[K comparable, V any](t *testing.T, v map[K]V, length int) {
	t.Helper()

	if l := len(v); l != length {
		t.Fatalf("Unexpected map length: %v", l)
	}
}
