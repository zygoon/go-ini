// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package testutil

import "testing"

// Equatable is the interface that wraps the Equal method.
type Equatable[T any] interface {
	Equal(T) bool
}

// ObjectEqual ensures that two objects compare equal with the Equal method.
func ObjectEqual[T Equatable[T]](t *testing.T, a, b T) {
	t.Helper()

	if !a.Equal(b) {
		t.Errorf("Objects differ %v != %v", a, b)
	}
}

// ObjectSliceEqual ensures that two slices have the same length and that their elements compare equal with the Equal method.
func ObjectSliceEqual[T Equatable[T]](t *testing.T, a, b []T) {
	t.Helper()

	slicesEqualLength(t, a, b)

	for i := range a {
		if !a[i].Equal(b[i]) {
			slicesDifferentError(t, i, a, b)
		}
	}
}
