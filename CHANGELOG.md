<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Zygmunt Krynicki
-->

# Upcoming release

- The module no longer depends on go-check.v1, reducing dependencies in all
  downstream projects.

# v1.1.0

- Comprehensive support for using slices! Slices of compatible types may be
  encoded into multiple and decoded back. Types which implement `TextMarshaler`
  and `TextUnmarshaler` interfaces continue to work as before. A slice type
  implementing the two interfaces still marshals into a single entry and back.
  A slice where each value implements those interfaces are also supported, and
  behave like slices with specialized representation.
- The function `ini.Encoder.Encode` can now fail with `ini.EncoderError` which
  encapsulates information about the structure and field that failed to encode
  correctly. The produced error messages are identical to what was returned in
  previous releases.
- Add support for `complex64` and `complex128` types. Complex values are
  encoded using `strconv.FormatComplex`.
- The `Encoder` can now insert comments for the entire encoded value, each
  individual section and each individual field. Use `Encoder.SetCommentFunc`
  to set the comment generator function. Use `Encoder.UseUnixComments` or
  `Encoder.UseWindowsComments` to set the preferred comment style. Use
  `Encoder.SetWrapColumn` to control comment wrapping behavior.
