// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ini_test

import (
	"bytes"
	"math"
	"reflect"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/zygoon/go-ini"
	"gitlab.com/zygoon/go-ini/internal/testutil"
)

func TestEncoder_EncodeNilValue(t *testing.T) {
	var buf bytes.Buffer

	enc := ini.NewEncoder(&buf)
	err := enc.Encode(nil)
	testutil.ErrorMatches(t, err, `cannot encode nil values`)
	testutil.ErrorIs(t, err, ini.ErrEncodeNil)
}

func TestEncoder_EncodeNonStructurePointer(t *testing.T) {
	var buf bytes.Buffer

	enc := ini.NewEncoder(&buf)
	testutil.ErrorMatches(t, enc.Encode(new(int)), `cannot encode values of type: \*int`)
}

func TestEncoder_SkipsUntaggedFields(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		Field int
	}

	enc := ini.NewEncoder(&buf)
	testutil.Ok(t, enc.Encode(&testType{Field: 42}))
	testutil.Equal(t, buf.String(), "")
}

func TestEncoder_EncodeSkipsUnexportedFields(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		field int `ini:"f"`
	}

	enc := ini.NewEncoder(&buf)
	testutil.Ok(t, enc.Encode(&testType{field: 42}))
	testutil.Equal(t, buf.String(), "")
}

func TestEncoder_EncodeVariousTypes(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		IntField    int    `ini:"IntField"`
		BoolField   bool   `ini:"BoolField"`
		StringField string `ini:"StringField"`
	}

	enc := ini.NewEncoder(&buf)
	value := &testType{
		IntField:    42,
		BoolField:   true,
		StringField: "hello world",
	}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), ""+
		"IntField=42\n"+
		"BoolField=true\n"+
		"StringField=hello world\n")
}

func TestEncoder_EncodeRespectsOmitEmptyTag(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		IntField    int    `ini:"IntField,omitempty"`
		BoolField   bool   `ini:"BoolField,omitempty"`
		StringField string `ini:"StringField,omitempty"`
	}

	enc := ini.NewEncoder(&buf)
	testutil.Ok(t, enc.Encode(&testType{}))
	testutil.Equal(t, buf.String(), "")
}

func TestEncoder_EncodeRespectsIniSections(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		IntField    int    `ini:"IntField,[Integers]"`
		BoolField   bool   `ini:"BoolField,[Booleans]"`
		StringField string `ini:"StringField,[Strings]"`
	}

	enc := ini.NewEncoder(&buf)
	value := &testType{
		IntField:    42,
		BoolField:   true,
		StringField: "hello world",
	}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), ""+
		"[Integers]\n"+
		"IntField=42\n"+
		"[Booleans]\n"+
		"BoolField=true\n"+
		"[Strings]\n"+
		"StringField=hello world\n")
}

func TestEncoder_EncodeKeepsTrackOfSections(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		IntField1 int  `ini:"IntField1,[Integers]"`
		IntField2 int  `ini:"IntField2"`
		IntField3 int  `ini:"IntField3,[Integers]"`
		BoolField bool `ini:"BoolField,[Booleans]"`
	}

	enc := ini.NewEncoder(&buf)
	value := &testType{
		IntField1: 1,
		IntField2: 2,
		IntField3: 3,
		BoolField: true,
	}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), ""+
		"[Integers]\n"+
		"IntField1=1\n"+
		"IntField2=2\n"+
		"IntField3=3\n"+
		"[Booleans]\n"+
		"BoolField=true\n")
}

func TestEncoder_EncodeSectionsAndOmittedEmptyFields(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		IntField1 int `ini:"IntField1,[Integers]"`
		// Field is omitted when value is zero, but the section name sticks.
		IntField2 int `ini:"IntField2,[Surprise],omitempty"`
		IntField3 int `ini:"IntField3"`
	}

	enc := ini.NewEncoder(&buf)
	value := &testType{
		IntField1: 1,
		IntField2: 0,
		IntField3: 3,
	}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), ""+
		"[Integers]\n"+
		"IntField1=1\n"+
		"[Surprise]\n"+
		"IntField3=3\n")
}

func TestEncoder_EncodeBool(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		Field bool `ini:"F"`
	}

	enc := ini.NewEncoder(&buf)
	value := &testType{Field: false}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), "F=false\n")
	buf.Reset()

	value = &testType{Field: true}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), "F=true\n")
	buf.Reset()
}

func Test_Encoder_EncodeInt(t *testing.T) {
	type testCase struct {
		value interface{}
		min   int64
		max   int64
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field int `ini:"F"`
			}{},
			min: math.MinInt,
			max: math.MaxInt,
		},
		{
			value: &struct {
				Field int8 `ini:"F"`
			}{},
			min: math.MinInt8,
			max: math.MaxInt8,
		},
		{
			value: &struct {
				Field int16 `ini:"F"`
			}{},
			min: math.MinInt16,
			max: math.MaxInt16,
		},
		{
			value: &struct {
				Field int32 `ini:"F"`
			}{},
			min: math.MinInt32,
			max: math.MaxInt32,
		},
		{
			value: &struct {
				Field int64 `ini:"F"`
			}{},
			min: math.MinInt64,
			max: math.MaxInt64,
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			var buf bytes.Buffer

			enc := ini.NewEncoder(&buf)

			reflect.ValueOf(tc.value).Elem().FieldByName("Field").SetInt(tc.min)
			testutil.Ok(t, enc.Encode(tc.value))
			testutil.Equal(t, buf.String(), "F="+strconv.FormatInt(tc.min, 10)+"\n")
			buf.Reset()

			reflect.ValueOf(tc.value).Elem().FieldByName("Field").SetInt(tc.max)
			testutil.Ok(t, enc.Encode(tc.value))
			testutil.Equal(t, buf.String(), "F="+strconv.FormatInt(tc.max, 10)+"\n")
		})
	}
}

func TestEncoder_EncodeUint(t *testing.T) {
	type testCase struct {
		value interface{}
		max   uint64
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field uint `ini:"F"`
			}{},
			max: math.MaxUint,
		},
		{
			value: &struct {
				Field uint8 `ini:"F"`
			}{},
			max: math.MaxUint8,
		},
		{
			value: &struct {
				Field uint16 `ini:"F"`
			}{},
			max: math.MaxUint16,
		},
		{
			value: &struct {
				Field uint32 `ini:"F"`
			}{},
			max: math.MaxUint32,
		},
		{
			value: &struct {
				Field uint64 `ini:"F"`
			}{},
			max: math.MaxUint64,
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			var buf bytes.Buffer

			enc := ini.NewEncoder(&buf)

			reflect.ValueOf(tc.value).Elem().FieldByName("Field").SetUint(tc.max)
			testutil.Ok(t, enc.Encode(tc.value))
			testutil.Equal(t, buf.String(), "F="+strconv.FormatUint(tc.max, 10)+"\n")
			buf.Reset()

			reflect.ValueOf(tc.value).Elem().FieldByName("Field").SetUint(0)
			testutil.Ok(t, enc.Encode(tc.value))
			testutil.Equal(t, buf.String(), "F=0\n")
		})
	}
}

func TestEncoder_EncodeFloat(t *testing.T) {
	type testCase struct {
		value   interface{}
		min     float64
		max     float64
		bitSize int
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field float32 `ini:"F"`
			}{},
			min:     math.SmallestNonzeroFloat32,
			max:     math.MaxFloat32,
			bitSize: 32,
		},
		{
			value: &struct {
				Field float64 `ini:"F"`
			}{},
			min:     math.SmallestNonzeroFloat64,
			max:     math.MaxFloat64,
			bitSize: 64,
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			var buf bytes.Buffer

			enc := ini.NewEncoder(&buf)

			reflect.ValueOf(tc.value).Elem().FieldByName("Field").SetFloat(tc.min)
			testutil.Ok(t, enc.Encode(tc.value))
			testutil.Equal(t, buf.String(), "F="+strconv.FormatFloat(tc.min, 'f', -1, tc.bitSize)+"\n")
			buf.Reset()

			reflect.ValueOf(tc.value).Elem().FieldByName("Field").SetFloat(tc.max)
			testutil.Ok(t, enc.Encode(tc.value))
			testutil.Equal(t, buf.String(), "F="+strconv.FormatFloat(tc.max, 'f', -1, tc.bitSize)+"\n")
		})
	}
}

func TestEncoder_EncodeComplex(t *testing.T) {
	type testCase struct {
		value   interface{}
		bitSize int
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field complex64 `ini:"F"`
			}{},
			bitSize: 64,
		},
		{
			value: &struct {
				Field complex128 `ini:"F"`
			}{},
			bitSize: 128,
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			const val = -42 + 15i

			var buf bytes.Buffer

			enc := ini.NewEncoder(&buf)

			reflect.ValueOf(tc.value).Elem().FieldByName("Field").SetComplex(val)
			testutil.Ok(t, enc.Encode(tc.value))
			testutil.Equal(t, buf.String(), "F=(-42+15i)\n")
		})
	}
}

func TestEncoder_EncodeString(t *testing.T) {
	type testCase struct {
		value   interface{}
		encoded string
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field string `ini:"F"`
			}{
				Field: "hello",
			},
			encoded: "F=hello\n",
		},
		{
			value: &struct {
				Field string `ini:"F"`
			}{
				Field: "hello\nnewline\nfriendly",
			},
			// The two spaces on each following line are to align with F= on the
			// first line, not pictured here.
			encoded: "F=hello\\\n  newline\\\n  friendly\n",
		},
		{
			value: &struct {
				Field string `ini:"F,quote"`
			}{
				Field: "hello",
			},
			encoded: "F=\"hello\"\n",
		},
		{
			value: &struct {
				Field string `ini:"F,quote"`
			}{
				Field: "hello\nnewline\nfriendly",
			},
			encoded: "F=\"hello\\nnewline\\nfriendly\"\n",
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			var buf bytes.Buffer

			enc := ini.NewEncoder(&buf)
			testutil.Ok(t, enc.Encode(tc.value))
			testutil.Equal(t, buf.String(), tc.encoded)
		})
	}
}

func TestEncoder_EncodeTextMarshaler(t *testing.T) {
	var buf bytes.Buffer

	enc := ini.NewEncoder(&buf)

	type testType struct {
		Field testEnum `ini:"F"`
	}

	value := &testType{Field: testFoo}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), "F=foo\n")

	buf.Reset()

	value = &testType{Field: testBar}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), "F=bar\n")

	err := enc.Encode(&testType{Field: testEnum(42)})
	testutil.ErrorMatches(t, err, `cannot encode testType.Field: cannot marshal testEnum: 42`)

	encErr := testutil.ErrorAs[*ini.EncoderError](t, err)
	testutil.Equal(t, encErr.StructName, "testType")
	testutil.Equal(t, encErr.FieldName, "Field")
	testutil.ErrorMatches(t, encErr.Err, "cannot marshal testEnum: 42")
}

func TestEncoder_EncodeIgnoredFields(t *testing.T) {
	var buf bytes.Buffer

	enc := ini.NewEncoder(&buf)

	type testType struct {
		Field bool
	}

	value := &testType{Field: true}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), "")
}

func TestEncodeIntSlice(t *testing.T) {
	var buf bytes.Buffer

	enc := ini.NewEncoder(&buf)

	type testType struct {
		Field []int `ini:"F"`
	}

	value := &testType{Field: []int{1, 2, 3}}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), "F=1\nF=2\nF=3\n")
}

// StringList is a slice type that also implements encoding.TextMarshaler and
// encoding.TextUnmarshaler. It is used to test that the encoder uses the
// TextMarshaler and TextUnmarshaler interfaces when they are available, even if
// the type is a slice.
type StringList []string

func (sl StringList) MarshalText() ([]byte, error) {
	return []byte(strings.Join([]string(sl), ",")), nil
}

func (sl *StringList) UnmarshalText(data []byte) error {
	*sl = StringList(strings.Split(string(data), ","))

	return nil
}

func (sl StringList) Equal(other StringList) bool {
	if len(sl) != len(other) {
		return false
	}

	for i := range sl {
		if sl[i] != other[i] {
			return false
		}
	}

	return true
}

func TestEncoder_EncodeStringList(t *testing.T) {
	var buf bytes.Buffer

	enc := ini.NewEncoder(&buf)

	type testType struct {
		Field StringList `ini:"F"`
	}

	value := &testType{Field: StringList{"a", "b", "c"}}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), "F=a,b,c\n")
}

func TestEncoder_EncodeSliceOfStringList(t *testing.T) {
	var buf bytes.Buffer

	enc := ini.NewEncoder(&buf)

	type testType struct {
		Field []StringList `ini:"F"`
	}

	value := &testType{Field: []StringList{{"a", "b", "c"}, {"x", "y", "z"}}}
	testutil.Ok(t, enc.Encode(value))
	testutil.Equal(t, buf.String(), "F=a,b,c\nF=x,y,z\n")
}

func TestEncoder_SetCommentFunc(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		Field string `ini:"Field,[Section]"`
	}

	enc := ini.NewEncoder(&buf)

	enc.SetCommentFunc(func(section, field string) string {
		switch section {
		case "":
			return "Global comment.\nThis comment spans multiple lines."
		case "Section":
			switch field {
			case "":
				return "Section comment."
			case "Field":
				return "Field comment."
			}
		}

		return ""
	})

	testutil.Ok(t, enc.Encode(testType{Field: "potato"}))
	testutil.Equal(t, buf.String(), `; Global comment.
; This comment spans multiple lines.
; Section comment.
[Section]
; Field comment.
Field=potato
`)
}

func TestEncoder_SetWrapColumn(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		Field string `ini:"Field,[Section]"`
	}

	enc := ini.NewEncoder(&buf)

	enc.SetWrapColumn(10)
	enc.SetCommentFunc(func(section, field string) string {
		switch section {
		case "":
			return "Global comment.\nThis comment spans multiple lines."
		case "Section":
			switch field {
			case "":
				return "Section comment."
			case "Field":
				return "Field comment."
			}
		}

		return ""
	})

	testutil.Ok(t, enc.Encode(testType{Field: "potato"}))
	testutil.Equal(t, buf.String(), `; Global
; comment.
; This
; comment
; spans
; multiple
; lines.
; Section
; comment.
[Section]
; Field
; comment.
Field=potato
`)
}

func TestEncoder_UseUnixComments(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		Field string `ini:"Field,[Section]"`
	}

	enc := ini.NewEncoder(&buf)

	enc.UseUnixComments()
	enc.SetCommentFunc(func(section, field string) string {
		if section == "" && field == "" {
			return "Global comment."
		}

		return ""
	})
	testutil.Ok(t, enc.Encode(testType{Field: "potato"}))
	testutil.Equal(t, buf.String(), `# Global comment.
[Section]
Field=potato
`)
}

func TestEncoder_UseWindowsComments(t *testing.T) {
	var buf bytes.Buffer

	type testType struct {
		Field string `ini:"Field,[Section]"`
	}

	enc := ini.NewEncoder(&buf)

	enc.UseWindowsComments()
	enc.SetCommentFunc(func(section, field string) string {
		if section == "" && field == "" {
			return "Global comment."
		}

		return ""
	})

	testutil.Ok(t, enc.Encode(testType{Field: "potato"}))
	testutil.Equal(t, buf.String(), `; Global comment.
[Section]
Field=potato
`)
}
