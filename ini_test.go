// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ini_test

import (
	"testing"

	"gitlab.com/zygoon/go-ini"
	"gitlab.com/zygoon/go-ini/internal/testutil"
)

func TestMarshalUnmarshal(t *testing.T) {
	type Person struct {
		Name string `ini:"Name,[Person]"`
		Age  int    `ini:"Age,omitempty"`
	}

	data, err := ini.Marshal(&Person{Name: "Bob", Age: 30})
	testutil.Ok(t, err)
	testutil.Equal(t, string(data), "[Person]\nName=Bob\nAge=30\n")

	var p Person
	err = ini.Unmarshal(data, &p)
	testutil.Ok(t, err)
	testutil.Equal(t, p, Person{Name: "Bob", Age: 30})
}
