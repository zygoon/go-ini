// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ini

import (
	"errors"
	"fmt"
)

var (
	// ErrEncodeNil reports attempt to encode a nil value
	ErrEncodeNil = errors.New("cannot encode nil values")
	// ErrEmptyFieldName records assignments with empty field name.
	ErrEmptyFieldName = errors.New("empty field name")
	// ErrUnterminatedContinuationAtEOF records unterminated multi-line entry.
	ErrUnterminatedContinuationAtEOF = errors.New("unterminated continuation at end of input")
)

// DecoderError records problem encountered during parsing.
type DecoderError struct {
	LineNo int
	Err    error
}

// Unwrap returns the error wrapped inside the decoder error.
func (decErr *DecoderError) Unwrap() error {
	return decErr.Err
}

// Error implements the error interface.
func (decErr *DecoderError) Error() string {
	return fmt.Sprintf("cannot decode line %d: %s", decErr.LineNo, decErr.Err)
}

// UnexpectedInputError records input not expected by the parser.
type UnexpectedInputError []byte

// Error implements the error interface.
func (e UnexpectedInputError) Error() string {
	return fmt.Sprintf("unexpected input: %q", string(e))
}

// UnknownSectionError records an unknown section.
//
// Unknown sections are ignored unless Decoder.DisallowUnknownSections is
// called on a new decoder instance.
type UnknownSectionError string

// SectionName returns the name of the section with '[' and ']' stripped out.
func (e UnknownSectionError) SectionName() string {
	return string(e)
}

// Error implements the error interface.
func (e UnknownSectionError) Error() string {
	return fmt.Sprintf("unknown section: %s", e.SectionName())
}

// UnknownFieldError records an unknown field.
//
// Unknown fields are ignored unless Decoder.DisallowUnknownFields is
// called on a new decoder instance.
type UnknownFieldError string

// FieldName returns the name of the field.
func (e UnknownFieldError) FieldName() string {
	return string(e)
}

// Error implements the error interface.
func (e UnknownFieldError) Error() string {
	return fmt.Sprintf("unknown field: %s", e.FieldName())
}

// FieldDecodeError records failed attempt to decode a structure field.
type FieldDecodeError struct {
	StructName string
	FieldName  string
	Err        error
}

// Error implements error.
func (e *FieldDecodeError) Error() string {
	return fmt.Sprintf("cannot decode %s.%s: %v", e.StructName, e.FieldName, e.Err)
}

// Unwrap returns the error wrapped by field decode error.
func (e *FieldDecodeError) Unwrap() error {
	return e.Err
}

// StringUnquoteError records problem with removing quoting marks from a string.
type StringUnquoteError struct {
	Err error
}

// Error implements the error interface.
func (e *StringUnquoteError) Error() string {
	return fmt.Sprintf("cannot unquote string: %v", e.Err)
}

// Unwrap returns the error wrapped inside the string unquote error.
func (e *StringUnquoteError) Unwrap() error {
	return e.Err
}

// EncoderError records problem encountered during encoding.
type EncoderError struct {
	StructName string
	FieldName  string
	Err        error
}

// Unwrap returns the error wrapped inside the decoder error.
func (e *EncoderError) Unwrap() error {
	return e.Err
}

// Error implements the error interface.
func (e *EncoderError) Error() string {
	return fmt.Sprintf("cannot encode %s.%s: %v", e.StructName, e.FieldName, e.Err)
}
