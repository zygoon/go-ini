// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ini_test

import (
	"bytes"
	"errors"
	"math"
	"reflect"
	"strconv"
	"testing"

	"gitlab.com/zygoon/go-ini"
	"gitlab.com/zygoon/go-ini/internal/testutil"
)

func TestDecoder_DecodeNonStructType(t *testing.T) {
	var value int

	dec := ini.NewDecoder(bytes.NewBufferString(""))
	testutil.ErrorMatches(t, dec.Decode(&value), "cannot decode values of type .*")
}

func TestDecoder_DecodeNonPointerType(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString(""))
	err := dec.Decode(struct{}{})
	testutil.ErrorMatches(t, err, "cannot decode values of type .*")
}

func TestDecoder_DecodeUseUnixComments(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("# This is an UNIX comment"))
	dec.UseUnixComments()
	testutil.Ok(t, dec.Decode(&struct{}{}))

	dec = ini.NewDecoder(bytes.NewBufferString("; This is a Windows comment"))
	dec.UseUnixComments()
	testutil.ErrorMatches(t, dec.Decode(&struct{}{}), `cannot decode line 1: unexpected input: "; This is a Windows comment"`)
}

func TestDecoder_UseWindowsComments(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("; This is a Windows comment"))
	dec.UseWindowsComments()
	testutil.Ok(t, dec.Decode(&struct{}{}))

	dec = ini.NewDecoder(bytes.NewBufferString("# This is an UNIX comment"))
	dec.UseWindowsComments()
	testutil.ErrorMatches(t, dec.Decode(&struct{}{}), `cannot decode line 1: unexpected input: "# This is an UNIX comment"`)
}

func TestDecoder_DecodeCommentTypes(t *testing.T) {
	for i, input := range []string{
		"# First comment\n# Second comment\n",
		"; First comment\n; Second comment\n",
		"# First comment\n; Second comment\n",
		"; First comment\n# Second comment\n",
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			dec := ini.NewDecoder(bytes.NewBufferString(input))
			testutil.Ok(t, dec.Decode(&struct{}{}))
		})
	}
}

func TestDecoder_DecodeContinuations(t *testing.T) {
	type testType struct {
		Field string `ini:"F"`
	}

	for i, input := range []string{
		"F=v a l u e",
		"F=v a l\\\nu e",
		"F=v a l\\\n  u e",
		"F=v a l\\\n  u e  ",
		"F=\\\nv a l u e",
		"F=\\\nv\\\na\\\nl\\\nu\\\ne",
		"F=\\\n# comment\nv\\\na\\\nl\\\n;comment\nu\\\ne",
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Logf("Input: %q", input)
			dec := ini.NewDecoder(bytes.NewBufferString(input))

			var value testType

			testutil.Ok(t, dec.Decode(&value))
			testutil.Equal(t, value.Field, "v a l u e")
		})
	}
}

func TestDecoder_DecodeContinuationsDangling(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("\\"))
	err := dec.Decode(&struct{}{})
	testutil.ErrorMatches(t, err, `cannot decode line 2: unterminated continuation at end of input`)
	testutil.Equal(t, errors.Unwrap(err), ini.ErrUnterminatedContinuationAtEOF)
}

func TestDecoder_DecodeEmptyLines(t *testing.T) {
	for i, input := range []string{
		"",
		"\n",
		"\n  \n",
		"\r\n",
		"\t",
		"\v",
		" ",
		"\r\n\t\v ",
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Logf("Input: %q", input)

			dec := ini.NewDecoder(bytes.NewBufferString(input))
			testutil.Ok(t, dec.Decode(&struct{}{}))
		})
	}
}

func TestDecoder_DecodeFieldWithoutValue(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("FieldWithoutValue"))
	testutil.ErrorMatches(t, dec.Decode(&struct{}{}), `cannot decode line 1: unexpected input: "FieldWithoutValue"`)
}

func TestDecoder_DecodeValueWithoutField(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("=ValueWithoutField"))
	err := dec.Decode(&struct{}{})
	testutil.ErrorMatches(t, err, `cannot decode line 1: empty field name`)
	testutil.Equal(t, errors.Unwrap(err), ini.ErrEmptyFieldName)
}

func TestDecoder_DisallowUnknownSections(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("[Section]\n"))
	dec.DisallowUnknownSections()
	err := dec.Decode(&struct{}{})
	testutil.ErrorMatches(t, err, `cannot decode line 1: unknown section: Section`)
	testutil.Equal(t, errors.Unwrap(err), error(ini.UnknownSectionError("Section")))
}

func TestDecoder_DisallowUnknownFields(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("Field=Value\n"))
	dec.DisallowUnknownFields()
	err := dec.Decode(&struct{}{})
	testutil.ErrorMatches(t, err, `cannot decode line 1: unknown field: Field`)
	testutil.Equal(t, errors.Unwrap(err), error(ini.UnknownFieldError("Field")))
}

func TestDecoder_SectionTracking(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("" +
		"[Integers]\n" +
		"IntField1=1\n" +
		"[Surprise]\n" +
		"IntField3=3\n"))

	type testType struct {
		IntField1 int `ini:"IntField1,[Integers]"`
		IntField2 int `ini:"IntField2,[Surprise],omitempty"`
		IntField3 int `ini:"IntField3"`
	}

	var value testType

	testutil.Ok(t, dec.Decode(&value))
	testutil.Equal(t, value, testType{IntField1: 1, IntField2: 0, IntField3: 3})
}

func TestDecoder_CaseInsensitivity(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("" +
		"[SECTION]\n" +
		"FIELD=ok\n"))
	dec.DisallowUnknownFields()
	dec.DisallowUnknownSections()

	type testType struct {
		Field string `ini:"Field,[Section]"`
	}

	var value testType

	testutil.Ok(t, dec.Decode(&value))
	testutil.Equal(t, value, testType{Field: "ok"})
}

func TestDecoder_CaseSensitivity(t *testing.T) {
	dec := ini.NewDecoder(bytes.NewBufferString("" +
		"[SECTION]\n" +
		"FIELD=ok\n"))
	dec.CaseSensitive()
	dec.DisallowUnknownFields()
	dec.DisallowUnknownSections()

	type testType struct {
		Field string `ini:"Field,[Section]"`
	}

	var value testType

	testutil.ErrorMatches(t, dec.Decode(&value), `cannot decode line 1: unknown section: SECTION`)

	dec = ini.NewDecoder(bytes.NewBufferString("" +
		"[Section]\n" +
		"FIELD=ok\n"))
	dec.CaseSensitive()
	dec.DisallowUnknownFields()
	dec.DisallowUnknownSections()

	testutil.ErrorMatches(t, dec.Decode(&value), `cannot decode line 2: unknown field: FIELD`)

	dec = ini.NewDecoder(bytes.NewBufferString("" +
		"[Section]\n" +
		"Field=ok\n"))
	dec.CaseSensitive()
	dec.DisallowUnknownFields()
	dec.DisallowUnknownSections()
	testutil.Ok(t, dec.Decode(&value))
	testutil.Equal(t, value, testType{Field: "ok"})
}

func TestDecoder_DecodeBoolean(t *testing.T) {
	type testType struct {
		Field bool `ini:"F"`
	}

	var value testType

	dec := ini.NewDecoder(bytes.NewBufferString("F=true"))
	testutil.Ok(t, dec.Decode(&value))
	testutil.Equal(t, value.Field, true)

	dec = ini.NewDecoder(bytes.NewBufferString("F=false"))
	testutil.Ok(t, dec.Decode(&value))
	testutil.Equal(t, value.Field, false)

	dec = ini.NewDecoder(bytes.NewBufferString("F=potato"))
	err := dec.Decode(&value)
	testutil.ErrorMatches(t, err, `cannot decode testType.Field: strconv.ParseBool: parsing "potato": invalid syntax`)
	numErr := testutil.ErrorAs[*strconv.NumError](t, err)
	testutil.Equal(t, numErr.Func, "ParseBool")
	testutil.Equal(t, numErr.Num, "potato")
	testutil.Equal(t, numErr.Err, strconv.ErrSyntax)
}

func TestDecoder_DecodeInt(t *testing.T) {
	type testCase struct {
		value interface{}
		min   int64
		max   int64
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field int `ini:"F"`
			}{},
			min: math.MinInt,
			max: math.MaxInt,
		},
		{
			value: &struct {
				Field int8 `ini:"F"`
			}{},
			min: math.MinInt8,
			max: math.MaxInt8,
		},
		{
			value: &struct {
				Field int16 `ini:"F"`
			}{},
			min: math.MinInt16,
			max: math.MaxInt16,
		},
		{
			value: &struct {
				Field int32 `ini:"F"`
			}{},
			min: math.MinInt32,
			max: math.MaxInt32,
		},
		{
			value: &struct {
				Field int64 `ini:"F"`
			}{},
			min: math.MinInt64,
			max: math.MaxInt64,
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			dec := ini.NewDecoder(bytes.NewBufferString("F=" + strconv.FormatInt(tc.min, 10)))
			testutil.Ok(t, dec.Decode(tc.value))
			testutil.Equal(t, reflect.ValueOf(tc.value).Elem().FieldByName("Field").Int(), tc.min)

			dec = ini.NewDecoder(bytes.NewBufferString("F=" + strconv.FormatInt(tc.max, 10)))
			testutil.Ok(t, dec.Decode(tc.value))
			testutil.Equal(t, reflect.ValueOf(tc.value).Elem().FieldByName("Field").Int(), tc.max)

			// We cannot compute math.MinInt64-1 without underflow.
			if tc.min > math.MinInt64 {
				dec = ini.NewDecoder(bytes.NewBufferString("F=" + strconv.FormatInt(tc.min-1, 10)))
				err := dec.Decode(tc.value)
				testutil.ErrorMatches(t, err, `cannot decode .Field: strconv.ParseInt: parsing "-[0-9]+": value out of range`)
				numErr := testutil.ErrorAs[*strconv.NumError](t, err)
				testutil.Equal(t, numErr.Func, "ParseInt")
				testutil.Equal(t, numErr.Num, strconv.FormatInt(tc.min-1, 10))
				testutil.Equal(t, numErr.Err, strconv.ErrRange)
			}

			// We cannot compute math.MaxInt64+1 without overflowing.
			if tc.max < math.MaxInt64 {
				dec = ini.NewDecoder(bytes.NewBufferString("F=" + strconv.FormatInt(tc.max+1, 10)))
				err := dec.Decode(tc.value)
				testutil.ErrorMatches(t, err, `cannot decode .Field: strconv.ParseInt: parsing "[0-9]+": value out of range`)
				numErr := testutil.ErrorAs[*strconv.NumError](t, err)
				testutil.Equal(t, numErr.Func, "ParseInt")
				testutil.Equal(t, numErr.Num, strconv.FormatInt(tc.max+1, 10))
				testutil.Equal(t, numErr.Err, strconv.ErrRange)
			}

			dec = ini.NewDecoder(bytes.NewBufferString("F=potato"))
			err := dec.Decode(tc.value)
			testutil.ErrorMatches(t, err, `cannot decode .Field: strconv.ParseInt: parsing "potato": invalid syntax`)
			numErr := testutil.ErrorAs[*strconv.NumError](t, err)
			testutil.Equal(t, numErr.Func, "ParseInt")
			testutil.Equal(t, numErr.Num, "potato")
			testutil.Equal(t, numErr.Err, strconv.ErrSyntax)
		})
	}
}

func TestDecoder_DecodeUint(t *testing.T) {
	type testCase struct {
		value interface{}
		max   uint64
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field uint `ini:"F"`
			}{},
			max: math.MaxUint,
		},
		{
			value: &struct {
				Field uint8 `ini:"F"`
			}{},
			max: math.MaxUint8,
		},
		{
			value: &struct {
				Field uint16 `ini:"F"`
			}{},
			max: math.MaxUint16,
		},
		{
			value: &struct {
				Field uint32 `ini:"F"`
			}{},
			max: math.MaxUint32,
		},
		{
			value: &struct {
				Field uint64 `ini:"F"`
			}{},
			max: math.MaxUint64,
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			dec := ini.NewDecoder(bytes.NewBufferString("F=0"))
			testutil.Ok(t, dec.Decode(tc.value))
			testutil.Equal(t, reflect.ValueOf(tc.value).Elem().FieldByName("Field").Uint(), 0)

			dec = ini.NewDecoder(bytes.NewBufferString("F=" + strconv.FormatUint(tc.max, 10)))
			testutil.Ok(t, dec.Decode(tc.value))
			testutil.Equal(t, reflect.ValueOf(tc.value).Elem().FieldByName("Field").Uint(), tc.max)

			// We cannot compute math.MaxUint64+1 without overflowing.
			if tc.max < math.MaxUint64 {
				dec = ini.NewDecoder(bytes.NewBufferString("F=" + strconv.FormatUint(tc.max+1, 10)))
				err := dec.Decode(tc.value)
				testutil.ErrorMatches(t, err, `cannot decode .Field: strconv.ParseUint: parsing "[0-9]+": value out of range`)
				e := testutil.ErrorAs[*strconv.NumError](t, err)
				testutil.Equal(t, e.Func, "ParseUint")
				testutil.Equal(t, e.Num, strconv.FormatUint(tc.max+1, 10))
				testutil.Equal(t, e.Err, strconv.ErrRange)
			}

			dec = ini.NewDecoder(bytes.NewBufferString("F=potato"))
			err := dec.Decode(tc.value)
			testutil.ErrorMatches(t, err, `cannot decode .Field: strconv.ParseUint: parsing "potato": invalid syntax`)
			e := testutil.ErrorAs[*strconv.NumError](t, err)
			testutil.Equal(t, e.Func, "ParseUint")
			testutil.Equal(t, e.Num, "potato")
			testutil.Equal(t, e.Err, strconv.ErrSyntax)
		})
	}
}

func TestDecoder_DecodeFloat(t *testing.T) {
	type testCase struct {
		value   interface{}
		min     float64
		max     float64
		bitSize int
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field float32 `ini:"F"`
			}{},
			min:     math.SmallestNonzeroFloat32,
			max:     math.MaxFloat32,
			bitSize: 32,
		},
		{
			value: &struct {
				Field float64 `ini:"F"`
			}{},
			min:     math.SmallestNonzeroFloat64,
			max:     math.MaxFloat64,
			bitSize: 64,
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			dec := ini.NewDecoder(bytes.NewBufferString("F=" + strconv.FormatFloat(tc.min, 'x', -1, tc.bitSize)))
			testutil.Ok(t, dec.Decode(tc.value))
			testutil.Equal(t, reflect.ValueOf(tc.value).Elem().FieldByName("Field").Float(), tc.min)

			dec = ini.NewDecoder(bytes.NewBufferString("F=" + strconv.FormatFloat(tc.max, 'x', -1, tc.bitSize)))
			testutil.Ok(t, dec.Decode(tc.value))
			testutil.Equal(t, reflect.ValueOf(tc.value).Elem().FieldByName("Field").Float(), tc.max)

			dec = ini.NewDecoder(bytes.NewBufferString("F=potato"))
			err := dec.Decode(tc.value)
			testutil.ErrorMatches(t, err, `cannot decode .Field: strconv.ParseFloat: parsing "potato": invalid syntax`)
			e := testutil.ErrorAs[*strconv.NumError](t, err)
			testutil.Equal(t, e.Func, "ParseFloat")
			testutil.Equal(t, e.Num, "potato")
			testutil.Equal(t, e.Err, strconv.ErrSyntax)
		})
	}
}

func TestDecoder_DecodeComplex(t *testing.T) {
	type testCase struct {
		value interface{}
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field complex64 `ini:"F"`
			}{},
		},
		{
			value: &struct {
				Field complex128 `ini:"F"`
			}{},
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			dec := ini.NewDecoder(bytes.NewBufferString("F=(1+2i)"))
			testutil.Ok(t, dec.Decode(tc.value))
			testutil.Equal(t, reflect.ValueOf(tc.value).Elem().FieldByName("Field").Complex(), 1+2i)

			dec = ini.NewDecoder(bytes.NewBufferString("F=potato"))
			err := dec.Decode(tc.value)
			testutil.ErrorMatches(t, err, `cannot decode .Field: strconv.ParseComplex: parsing "potato": invalid syntax`)
			e := testutil.ErrorAs[*strconv.NumError](t, err)
			testutil.Equal(t, e.Func, "ParseComplex")
			testutil.Equal(t, e.Num, "potato")
			testutil.Equal(t, e.Err, strconv.ErrSyntax)
		})
	}
}

func TestDecoder_DecodeString(t *testing.T) {
	type testCase struct {
		value      interface{}
		textIn     string
		textOut    string
		errPattern string
	}

	for i, tc := range []testCase{
		{
			value: &struct {
				Field string `ini:"F"`
			}{},
			textIn:  "hello world",
			textOut: "hello world",
		},
		{
			value: &struct {
				Field string `ini:"F,quote"`
			}{},
			textIn:  `"hello world"`,
			textOut: "hello world",
		},
		{
			value: &struct {
				Field string `ini:"F,quote"`
			}{},
			textIn:     `"hello world`,
			errPattern: `cannot decode .Field: cannot unquote string: invalid syntax`,
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			dec := ini.NewDecoder(bytes.NewBufferString("F=" + tc.textIn))
			if tc.errPattern == "" {
				testutil.Ok(t, dec.Decode(tc.value))
				testutil.Equal(t, reflect.ValueOf(tc.value).Elem().FieldByName("Field").String(), tc.textOut)
			} else {
				err := dec.Decode(tc.value)
				testutil.ErrorMatches(t, err, tc.errPattern)
				testutil.ErrorIs(t, err, strconv.ErrSyntax)
			}
		})
	}

	dec := ini.NewDecoder(bytes.NewBufferString(`F="broken`))
	err := dec.Decode(&struct {
		Field string `ini:"F,quote"`
	}{})
	testutil.NotOk(t, err)
	testutil.ErrorMatches(t, errors.Unwrap(err), `cannot unquote string: invalid syntax`)
}

func TestDecoder_DecodeTextUnmarshaler(t *testing.T) {
	type testType struct {
		Field testEnum `ini:"F"`
	}

	var value testType

	dec := ini.NewDecoder(bytes.NewBufferString("F=foo"))
	testutil.Ok(t, dec.Decode(&value))
	testutil.Equal(t, value.Field, testFoo)

	dec = ini.NewDecoder(bytes.NewBufferString("F=bar"))
	testutil.Ok(t, dec.Decode(&value))
	testutil.Equal(t, value.Field, testBar)

	dec = ini.NewDecoder(bytes.NewBufferString("F=froz"))
	testutil.ErrorMatches(t, dec.Decode(&value), `cannot decode testType.Field: cannot unmarshal testEnum: unknown value: "froz"`)
}

func TestDecoder_DecodeIgnoresFieldsWithoutIniStructTag(t *testing.T) {
	type testType struct {
		Field bool // No ini struct tag
	}

	var value testType

	dec := ini.NewDecoder(bytes.NewBufferString(""))
	testutil.Ok(t, dec.Decode(&value))
	testutil.Equal(t, value.Field, false)

	dec = ini.NewDecoder(bytes.NewBufferString("Field=true"))
	testutil.Ok(t, dec.Decode(&value))
	testutil.Equal(t, value.Field, false)
}

func TestDecoder_DecodeIntSlice(t *testing.T) {
	type T struct {
		Field []int `ini:"F"`
	}

	var value T

	dec := ini.NewDecoder(bytes.NewBufferString("F=1\nF=2\nF=3\n"))
	testutil.Ok(t, dec.Decode(&value))
	testutil.SlicesEqual(t, value.Field, []int{1, 2, 3})
}

func TestDecoder_DecodeStringList(t *testing.T) {
	type T struct {
		Field StringList `ini:"F"`
	}

	var value T

	dec := ini.NewDecoder(bytes.NewBufferString("F=a,b,c\n"))
	testutil.Ok(t, dec.Decode(&value))
	testutil.ObjectEqual(t, value.Field, StringList{"a", "b", "c"})
}

func TestDecoder_DecodeSliceOfStringList(t *testing.T) {
	type T struct {
		Field []StringList `ini:"F"`
	}

	var value T

	dec := ini.NewDecoder(bytes.NewBufferString("F=a,b,c\nF=x,y,z\n"))
	testutil.Ok(t, dec.Decode(&value))
	testutil.ObjectSliceEqual(t, value.Field, []StringList{{"a", "b", "c"}, {"x", "y", "z"}})
}
