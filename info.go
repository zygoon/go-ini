// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ini

import (
	"encoding"
	"fmt"
	"reflect"
	"strings"
)

// iniFieldInfo describes a single structure field along with ini section data.
type iniFieldInfo struct {
	Section   string // Name of the ini section, without [ and ]
	Name      string // Name of the ini key
	OmitEmpty bool   // Omit zero values when marshaling
	Quote     bool   // Quote value when marshaling

	FieldIndex []int // Index valid for Value.Field(int)

	StructName string // Name of the structure type in Go
	FieldName  string // Name of the structure field in Go
}

var (
	textMarshalerType   = reflect.TypeOf((*encoding.TextMarshaler)(nil)).Elem()
	textUnmarshalerType = reflect.TypeOf((*encoding.TextUnmarshaler)(nil)).Elem()
)

// iniSliceOfType returns a slice with information about fields in a given type.
//
// This function is used by the encoder and decoder.
func iniSliceOfType(structType reflect.Type) ([]iniFieldInfo, error) {
	result := make([]iniFieldInfo, 0, structType.NumField())
	section := "" // keeps track of current section name

	for i := 0; i < structType.NumField(); i++ {
		structField := structType.Field(i)
		if !structField.IsExported() {
			continue
		}

		tagValue, ok := structField.Tag.Lookup("ini")
		if !ok || tagValue == "-" {
			continue
		}

		var name, newSection string

		var omitempty, quote bool

		iniTags := strings.Split(tagValue, ",")
		for i, tag := range iniTags {
			if i == 0 {
				name = tag
				continue
			}

			switch tag {
			case "omitempty":
				omitempty = true
			case "quote":
				quote = true
			default:
				if strings.HasPrefix(tag, "[") && strings.HasSuffix(tag, "]") {
					newSection = tag[1 : len(tag)-1]
				} else {
					return nil, fmt.Errorf("ini error: field %s contains unsupported struct-tag: %q", structField.Name, tag)
				}
			}
		}

		if name == "" {
			return nil, fmt.Errorf("ini error: field %s lacks struct-tag with field name", structField.Name)
		}

		if newSection != "" && section != newSection {
			section = newSection
		}

		result = append(result, iniFieldInfo{
			Section:   section,
			Name:      name,
			OmitEmpty: omitempty,
			Quote:     quote,

			FieldIndex: structField.Index,

			StructName: structType.Name(),
			FieldName:  structField.Name,
		})

		structFieldType := structField.Type

		if structFieldType.Kind() == reflect.Slice {
			structFieldType = structFieldType.Elem()
		}

		switch structFieldType.Kind() {
		case reflect.Bool:
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		case reflect.Float32, reflect.Float64:
		case reflect.Complex64, reflect.Complex128:
		case reflect.String:
		default:
			// Check if the field can be an encoding.TextMarshaler and encoding.TextUnmarshaler.
			if !structFieldType.Implements(textMarshalerType) {
				return nil, fmt.Errorf("ini error: field %s non-basic type does not implement encoding.TextMarshaler", structField.Name)
			}

			// Since TextUnmarshaler requires practical implementations to be called by pointer,
			// perform the check on the pointer to the type of the field.
			if !reflect.PtrTo(structFieldType).Implements(textUnmarshalerType) {
				return nil, fmt.Errorf("ini error: field %s non-basic type does not implement encoding.TextUnmarshaler", structField.Name)
			}
		}
	}

	return result, nil
}

// iniMapOfType returns a map indexed by section and field name with to the info slice of a given type.
//
// This function is used by the decoder.
func iniMapOfType(structType reflect.Type, caseSensitive bool) (slice []iniFieldInfo, indexMap map[string]map[string]int, err error) {
	slice, err = iniSliceOfType(structType)
	indexMap = make(map[string]map[string]int, len(slice))

	for idx, info := range slice {
		section := info.Section
		name := info.Name

		if !caseSensitive {
			section = strings.ToLower(section)
			name = strings.ToLower(name)
		}

		if indexMap[section] == nil {
			indexMap[section] = make(map[string]int)
		}

		indexMap[section][name] = idx
	}

	return slice, indexMap, err
}
