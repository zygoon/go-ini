// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ini_test

import (
	"encoding"
	"fmt"
)

type testEnum int

const (
	testFoo testEnum = iota + 1
	testBar
)

func (v testEnum) MarshalText() ([]byte, error) {
	switch v {
	case testFoo:
		return []byte("foo"), nil
	case testBar:
		return []byte("bar"), nil
	default:
		return nil, fmt.Errorf("cannot marshal testEnum: %d", int(v))
	}
}

func (v *testEnum) UnmarshalText(data []byte) error {
	switch s := string(data); s {
	case "foo":
		*v = testFoo
	case "bar":
		*v = testBar
	default:
		return fmt.Errorf("cannot unmarshal testEnum: unknown value: %q", s)
	}

	return nil
}

var _ encoding.TextUnmarshaler = (*testEnum)(nil)
var _ encoding.TextMarshaler = (*testEnum)(nil)
