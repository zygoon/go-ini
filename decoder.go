// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ini

import (
	"bufio"
	"bytes"
	"encoding"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"
)

// Decoder reads INI values from an input stream.
type Decoder struct {
	r                       io.Reader
	disallowUnknownFields   bool
	disallowUnknownSections bool
	caseSensitive           bool
	commentRune             rune
}

// NewDecoder returns a new decoder that reads from r.
func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{r: r}
}

// DisallowUnknownSections makes the decoder reject unknown section names.
func (dec *Decoder) DisallowUnknownSections() {
	dec.disallowUnknownSections = true
}

// DisallowUnknownFields makes the decoder reject unknown field names.
func (dec *Decoder) DisallowUnknownFields() {
	dec.disallowUnknownFields = true
}

// UseUnixComments makes the decoder recognize comments introduced by '#'.
func (dec *Decoder) UseUnixComments() {
	dec.commentRune = '#'
}

// UseWindowsComments makes the decoder recognize comments introduced by ';'.
func (dec *Decoder) UseWindowsComments() {
	dec.commentRune = ';'
}

// CaseSensitive makes the decoder case-sensitive for both sections and field names.
func (dec *Decoder) CaseSensitive() {
	dec.caseSensitive = true
}

// Decode decodes a structure from the INI representation.
//
// The argument must be a pointer to a structure.
//
// Comments introduced with '#' or ';' are ignored. If a preference for certain
// style has been set, then only such comments are recognized.
//
// See the documentation of Unmarshal more details.
func (dec *Decoder) Decode(v interface{}) error {
	if reflect.TypeOf(v).Kind() != reflect.Ptr || reflect.TypeOf(v).Elem().Kind() != reflect.Struct {
		return fmt.Errorf("cannot decode values of type %T", v)
	}

	structValue := reflect.Indirect(reflect.ValueOf(v))
	structType := structValue.Type()

	iniFieldSlice, iniFieldIndexMap, err := iniMapOfType(structType, dec.caseSensitive)
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(dec.r)

	var sectionRealName, sectionLookupName string

	var accumulationBuf bytes.Buffer

	var accumulating bool

	var lineno int

	for lineno = 1; scanner.Scan(); lineno++ {
		rawLine := scanner.Bytes()

		// Trim whitespace. In general whitespace is irrelevant, including
		// around the '=' rune in key=value pairs below.
		rawLine = bytes.TrimSpace(rawLine)

		// Skip lines that are entirely empty.
		if len(rawLine) == 0 {
			continue
		}

		// Skip comments. Those take priority over line continuations, allowing comments
		// in the middle of input. If a preference was set, recognize only specific type.
		// If no preference was set recognize both '#' and ';' comments, assuming they
		// are the first rune of a line.
		commentFound := false

		switch dec.commentRune {
		case '#':
			commentFound = bytes.HasPrefix(rawLine, []byte("#"))
		case ';':
			commentFound = bytes.HasPrefix(rawLine, []byte(";"))
		default:
			commentFound = bytes.HasPrefix(rawLine, []byte("#")) || bytes.HasPrefix(rawLine, []byte(";"))
		}

		if commentFound {
			continue
		}

		// Handle continuations from previous line. Long lines can be split into
		// multiple sections, each but the last one terminated with a '\' rune.
		// If the continuation buffer is not empty, then the first line without
		// the continuation rune terminates the sequence.
		if bytes.HasSuffix(rawLine, []byte("\\")) {
			accumulationBuf.Write(rawLine[:len(rawLine)-1])
			accumulationBuf.WriteRune(' ')

			accumulating = true

			continue
		} else if accumulating {
			accumulationBuf.Write(rawLine)
			rawLine = accumulationBuf.Bytes()
			accumulationBuf.Reset()
			accumulating = false
		}

		// Handle [sections].
		if bytes.HasPrefix(rawLine, []byte(`[`)) && bytes.HasSuffix(rawLine, []byte(`]`)) {
			sectionRealName = string(rawLine[1 : len(rawLine)-1])
			sectionLookupName = sectionRealName

			if !dec.caseSensitive {
				sectionLookupName = strings.ToLower(sectionLookupName)
			}

			if _, ok := iniFieldIndexMap[sectionLookupName]; !ok && dec.disallowUnknownSections {
				return &DecoderError{LineNo: lineno, Err: UnknownSectionError(sectionRealName)}
			}

			continue
		}

		// Handle key=value pairs.
		if idx := bytes.IndexRune(rawLine, '='); idx != -1 {
			fieldRealName := string(bytes.TrimSpace(rawLine[:idx]))
			fieldLookupName := fieldRealName

			if len(fieldRealName) == 0 {
				return &DecoderError{LineNo: lineno, Err: ErrEmptyFieldName}
			}

			fieldRawValue := bytes.TrimSpace(rawLine[idx+1:])

			if !dec.caseSensitive {
				fieldLookupName = strings.ToLower(fieldLookupName)
			}

			if iniFieldIndex, ok := iniFieldIndexMap[sectionLookupName][fieldLookupName]; ok {
				if err := dec.decodeField(fieldRawValue, structType, &structValue, &iniFieldSlice[iniFieldIndex]); err != nil {
					return err
				}
			} else if dec.disallowUnknownFields {
				return &DecoderError{LineNo: lineno, Err: UnknownFieldError(fieldRealName)}
			}

			continue
		}

		// Report everything else as syntax error.
		return &DecoderError{LineNo: lineno, Err: UnexpectedInputError(rawLine)}
	}

	if accumulating {
		return &DecoderError{LineNo: lineno, Err: ErrUnterminatedContinuationAtEOF}
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func (dec *Decoder) decodeField(valueData []byte, _ reflect.Type, structValue *reflect.Value, info *iniFieldInfo) (err error) {
	v, err := structValue.FieldByIndexErr(info.FieldIndex)
	if err != nil {
		return err
	}

	if v.CanAddr() {
		if v := v.Addr(); v.CanInterface() {
			if v, ok := v.Interface().(encoding.TextUnmarshaler); ok {
				if err := v.UnmarshalText(valueData); err != nil {
					return &FieldDecodeError{StructName: info.StructName, FieldName: info.FieldName, Err: err}
				}

				return nil
			}
		}
	}

	if v.Kind() == reflect.Slice {
		if v.IsNil() {
			v = reflect.MakeSlice(v.Type(), 0, 1)
		}

		item := reflect.New(v.Type().Elem()).Elem()

		if err := dec.decodeFieldValue(valueData, item, info); err != nil {
			return &FieldDecodeError{StructName: info.StructName, FieldName: info.FieldName, Err: err}
		}

		structValue.FieldByIndex(info.FieldIndex).Set(reflect.Append(v, item))

		return nil
	}

	if err := dec.decodeFieldValue(valueData, v, info); err != nil {
		return &FieldDecodeError{StructName: info.StructName, FieldName: info.FieldName, Err: err}
	}

	return nil
}

func (dec *Decoder) decodeFieldValue(valueData []byte, v reflect.Value, info *iniFieldInfo) error {
	if v.CanAddr() {
		if v := v.Addr(); v.CanInterface() {
			if v, ok := v.Interface().(encoding.TextUnmarshaler); ok {
				return v.UnmarshalText(valueData)
			}
		}
	}

	switch kind := v.Kind(); kind {
	case reflect.Bool:
		boolValue, err := strconv.ParseBool(string(valueData))
		if err != nil {
			return err
		}

		v.SetBool(boolValue)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		bitSize := 0

		switch kind {
		case reflect.Int8:
			bitSize = 8
		case reflect.Int16:
			bitSize = 16
		case reflect.Int32:
			bitSize = 32
		case reflect.Int64:
			bitSize = 64
		}

		intValue, err := strconv.ParseInt(string(valueData), 10, bitSize)
		if err != nil {
			return err
		}

		v.SetInt(intValue)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		bitSize := 0

		switch kind {
		case reflect.Uint8:
			bitSize = 8
		case reflect.Uint16:
			bitSize = 16
		case reflect.Uint32:
			bitSize = 32
		case reflect.Uint64:
			bitSize = 64
		}

		uintValue, err := strconv.ParseUint(string(valueData), 10, bitSize)
		if err != nil {
			return err
		}

		v.SetUint(uintValue)
	case reflect.Float32, reflect.Float64:
		bitSize := 32
		if kind == reflect.Float64 {
			bitSize = 64
		}

		floatValue, err := strconv.ParseFloat(string(valueData), bitSize)
		if err != nil {
			return err
		}

		v.SetFloat(floatValue)
	case reflect.Complex64, reflect.Complex128:
		bitSize := 64
		if kind == reflect.Complex128 {
			bitSize = 128
		}

		complexValue, err := strconv.ParseComplex(string(valueData), bitSize)
		if err != nil {
			return err
		}

		v.SetComplex(complexValue)
	case reflect.String:
		strValue := string(valueData)

		if info.Quote {
			unquotedStrValue, err := strconv.Unquote(strValue)
			if err != nil {
				return &StringUnquoteError{Err: err}
			}

			strValue = unquotedStrValue
		}

		v.SetString(strValue)
	default:
		// This can never happen if iniSliceOfType is in sync.
		panic(fmt.Sprintf("cannot decode value of unsupported kind %v", kind))
	}

	return nil
}
